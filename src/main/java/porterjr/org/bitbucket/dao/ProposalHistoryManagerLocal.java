package porterjr.org.bitbucket.dao;

import javax.ejb.Local;

import porterjr.org.bitbucket.dto.proposal.ProposalHistory;
import porterjr.org.bitbucket.dto.proposal.ProposalHistoryRecords;

@Local
public interface ProposalHistoryManagerLocal {

	void addProposalHistory(ProposalHistory proposalHistory);
	
	/**
	 * 
	 * @param id of proposal which history we want to fetch
	 * @return history records of particular proposal
	 */
	ProposalHistoryRecords getProposalHistory(long id);
}
