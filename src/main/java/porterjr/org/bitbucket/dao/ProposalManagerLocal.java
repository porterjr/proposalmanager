package porterjr.org.bitbucket.dao;
import java.util.List;

import javax.ejb.Local;

import porterjr.org.bitbucket.dto.proposal.Proposal;
import porterjr.org.bitbucket.dto.proposal.constant.PROPOSAL_STATE;

@Local
public interface ProposalManagerLocal {
	
	Proposal updateProposal(long id, String name, PROPOSAL_STATE state, String description);
	
	Proposal updateProposal(long id, String name, PROPOSAL_STATE state);
	
	Proposal updateProposal(long id, String description);
	
	Proposal addProposal(String name, String description);
	
	Proposal getProposal(long id);
	
	List<Proposal> getAllProposalsWithRange(int range, boolean filteredByNameAndState);
}
