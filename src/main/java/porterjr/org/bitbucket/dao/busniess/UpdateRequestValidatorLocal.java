package porterjr.org.bitbucket.dao.busniess;

import javax.ejb.Local;

@Local
public interface UpdateRequestValidatorLocal {

	boolean isRequestValid(UriWrapper uri);
	
}
