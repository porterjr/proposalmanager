package porterjr.org.bitbucket.dao.busniess;

import javax.ejb.EJB;
import javax.enterprise.event.Observes;

import porterjr.org.bitbucket.dao.ProposalHistoryManagerLocal;
import porterjr.org.bitbucket.dto.proposal.ProposalHistory;

public class HistoryAppender {
	
	@EJB
	private ProposalHistoryManagerLocal proposaHistoryManager;
	
	@SuppressWarnings("unused")
	private void addToHistoryOfProposal(@Observes ProposalHistory proposalHistory) {
		proposaHistoryManager.addProposalHistory(proposalHistory);
	}
}
