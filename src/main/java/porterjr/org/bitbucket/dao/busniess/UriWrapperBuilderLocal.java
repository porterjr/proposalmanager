package porterjr.org.bitbucket.dao.busniess;

import java.net.URI;

import javax.ejb.Local;

@Local
public interface UriWrapperBuilderLocal {

	UriWrapper buildUriWrapper(URI uri);
	
}
