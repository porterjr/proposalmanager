package porterjr.org.bitbucket.dao.busniess;

import javax.ejb.Local;

import porterjr.org.bitbucket.dto.proposal.constant.PROPOSAL_STATE;

@Local
public interface ProposalStateValidatorLocal {

	boolean isMoveFromBeginningToTargetStatePossible(PROPOSAL_STATE beginningState, PROPOSAL_STATE targetState);
	
}
