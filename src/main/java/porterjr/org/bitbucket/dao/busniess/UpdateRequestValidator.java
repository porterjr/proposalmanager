package porterjr.org.bitbucket.dao.busniess;

import static porterjr.org.bitbucket.services.constant.UpdateProposalPaths.ACCEPT_PROPOSAL;
import static porterjr.org.bitbucket.services.constant.UpdateProposalPaths.EDIT_DESCRIPTION_REQUEST;
import static porterjr.org.bitbucket.services.constant.UpdateProposalPaths.FULL_REQUEST;
import static porterjr.org.bitbucket.services.constant.UpdateProposalPaths.PUBLISH_PROPOSAL;
import static porterjr.org.bitbucket.services.constant.UpdateProposalPaths.REJECT_PROPOSAL;
import static porterjr.org.bitbucket.services.constant.UpdateProposalPaths.REMOVE_PROPOSAL;
import static porterjr.org.bitbucket.services.constant.UpdateProposalPaths.VERIFY_PROPOSAL;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import porterjr.org.bitbucket.dao.ProposalManagerLocal;
import porterjr.org.bitbucket.dto.proposal.Proposal;
import porterjr.org.bitbucket.dto.proposal.constant.PROPOSAL_STATE;

@Stateless
public class UpdateRequestValidator implements UpdateRequestValidatorLocal {
	private final static String ID_PARAM_REQUEST_REGEXP = "id=(\\d+)";
	private Map<String,PROPOSAL_STATE> mapperRestMethodToState = new HashMap<>();

	@PostConstruct
	private void initMap() {
		mapperRestMethodToState.put(PUBLISH_PROPOSAL, PROPOSAL_STATE.PUBLISHED);
		mapperRestMethodToState.put(VERIFY_PROPOSAL, PROPOSAL_STATE.VERIFIED);
		mapperRestMethodToState.put(REMOVE_PROPOSAL, PROPOSAL_STATE.DELETED);
		mapperRestMethodToState.put(REJECT_PROPOSAL, PROPOSAL_STATE.REJECTED);
		mapperRestMethodToState.put(ACCEPT_PROPOSAL, PROPOSAL_STATE.ACCEPTED);
	}
	
	@EJB
	private ProposalManagerLocal proposalManager;
	
	@EJB
	private ProposalStateValidatorLocal proposalStateValidator;
	
	@Override
	public boolean isRequestValid(UriWrapper uri) {
		long idProposal = -1;
		if(isUpdateRequest((UriWrapperImpl)uri)){
			idProposal = idProposalWhichCanBeFetchedFromUri((UriWrapperImpl)uri);
			if(idProposal==-1)
				return false;
			return isUpdateToStateValid((UriWrapperImpl)uri,idProposal) || isPossibleDescriptionEdition((UriWrapperImpl)uri,idProposal);
		}
		return true;
	}
	private boolean isUpdateRequest(UriWrapperImpl uri) {
		return uri.getPath().contains(FULL_REQUEST);
	}
	
	private boolean isUpdateToStateValid(UriWrapperImpl uri, long id) {
		Proposal proposal = getProposalBasedOnId(id);
		String fullRequestPath = uri.toString();
		PROPOSAL_STATE actualState = PROPOSAL_STATE.NONE;
		if(proposal!=null)
			actualState = proposal.getState();
		return proposalStateValidator.isMoveFromBeginningToTargetStatePossible(actualState, getStateToWhichRequestWantUpdate(fullRequestPath));
	}
	
	private boolean isPossibleDescriptionEdition(UriWrapperImpl uri, long id) {
		if(!uri.getPath().contains(EDIT_DESCRIPTION_REQUEST))
			return false;
		Proposal proposal = getProposalBasedOnId(id);
		PROPOSAL_STATE actualState = PROPOSAL_STATE.NONE;
		if(proposal!=null)
			actualState = proposal.getState();
		return (actualState==PROPOSAL_STATE.CREATED || actualState == PROPOSAL_STATE.VERIFIED); 
	}
	
	private long idProposalWhichCanBeFetchedFromUri(UriWrapperImpl uri) {
		Pattern pattern = Pattern.compile(ID_PARAM_REQUEST_REGEXP);
		Matcher matcher  = pattern.matcher(uri.getQuery());
		long id=-1;
		if(matcher.find())
			id = Long.parseLong(matcher.group(1));
		return id;
	}
	
	private Proposal getProposalBasedOnId(long id) {
		return proposalManager.getProposal(id);
	}
	
	private PROPOSAL_STATE getStateToWhichRequestWantUpdate(String path) {
		for(String state : mapperRestMethodToState.keySet()) {
			if(path.contains(state))
				return mapperRestMethodToState.get(state);
		}
		return PROPOSAL_STATE.NONE;
	}
}
