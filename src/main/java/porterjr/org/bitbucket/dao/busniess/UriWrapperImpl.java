package porterjr.org.bitbucket.dao.busniess;

import java.net.URI;

/*
 * used for wrap final architecture of URI 
 */
//TODO check if it can be omitted and use pure URI
public  class UriWrapperImpl implements UriWrapper {
	private static final long serialVersionUID = -1609015354885413920L;
	private final URI uriToWrap;
	
	public UriWrapperImpl(URI uriToWrap) {
		this.uriToWrap = uriToWrap;
	}
	
	public String getQuery() {
		return uriToWrap.getQuery();
	}
	
	public String getPath() {
		return uriToWrap.getPath();
	}
	
	@Override
	public String toString() {
		return uriToWrap.toString();
	}
}
