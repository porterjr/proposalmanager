package porterjr.org.bitbucket.dao.busniess;

import static porterjr.org.bitbucket.dto.proposal.constant.PROPOSAL_STATE.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

import porterjr.org.bitbucket.dto.proposal.constant.PROPOSAL_STATE;

@Stateless
public class ProposalStateValidator implements ProposalStateValidatorLocal {

	private Map<PROPOSAL_STATE,Set<PROPOSAL_STATE>> proposalStateWithAllowedTransitions; 
	
	@PostConstruct
	public void init() {
		proposalStateWithAllowedTransitions = new HashMap<>();
		proposalStateWithAllowedTransitions.put(CREATED, new HashSet<PROPOSAL_STATE>(Arrays.asList(DELETED,VERIFIED)));
		proposalStateWithAllowedTransitions.put(DELETED, new HashSet<PROPOSAL_STATE>());
		proposalStateWithAllowedTransitions.put(VERIFIED, new HashSet<PROPOSAL_STATE>(Arrays.asList(REJECTED,ACCEPTED)));
		proposalStateWithAllowedTransitions.put(REJECTED, new HashSet<PROPOSAL_STATE>());
		proposalStateWithAllowedTransitions.put(ACCEPTED, new HashSet<PROPOSAL_STATE>(Arrays.asList(PUBLISHED,REJECTED)));
		proposalStateWithAllowedTransitions.put(PUBLISHED, new HashSet<PROPOSAL_STATE>());
		proposalStateWithAllowedTransitions.put(NONE, new HashSet<PROPOSAL_STATE>());
	}
	
	
	@Override
	public boolean isMoveFromBeginningToTargetStatePossible(PROPOSAL_STATE beginningState, PROPOSAL_STATE finalState) {
		return proposalStateWithAllowedTransitions.get(beginningState).contains(finalState);
	}

}
