package porterjr.org.bitbucket.dao.busniess;

import java.net.URI;

import javax.ejb.Stateless;

@Stateless
public class UriWrapperBuilder implements UriWrapperBuilderLocal {

	@Override
	public UriWrapper buildUriWrapper(URI uri) {
		return new UriWrapperImpl(uri);
	}

}
