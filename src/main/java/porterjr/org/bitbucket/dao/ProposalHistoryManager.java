package porterjr.org.bitbucket.dao;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import porterjr.org.bitbucket.dto.proposal.ProposalHistory;
import porterjr.org.bitbucket.dto.proposal.ProposalHistoryRecords;

@Stateless
public class ProposalHistoryManager implements ProposalHistoryManagerLocal {

	@Inject
	private EntityManager em;
	
	@Override
	public void addProposalHistory(ProposalHistory proposalHistory) {
		em.persist(proposalHistory);
	}

	@Override
	public ProposalHistoryRecords getProposalHistory(long id) {
		TypedQuery<ProposalHistory> query = em.createNamedQuery("ProposalHistory.findProposalHistory", ProposalHistory.class);
		query.setParameter("id_proposal", id);
		return new ProposalHistoryRecords(query.getResultList());
	}
}
