package porterjr.org.bitbucket.dao.factory;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class EntityFactory {

	@PersistenceContext(unitName="proposalManagementApp")
	private EntityManager em;
	
	@Produces
	public EntityManager produceEntityManager() {
		return em;
	}
	
}
