package porterjr.org.bitbucket.dao;

import static porterjr.org.bitbucket.dto.proposal.constant.ProposalQuery.FIND_AMOUNT_ORDER_BY_STATE_AND_NAME_QUERY_NAME;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import static porterjr.org.bitbucket.dto.proposal.constant.ProposalQuery.*;
import porterjr.org.bitbucket.dto.proposal.Proposal;
import porterjr.org.bitbucket.dto.proposal.constant.PROPOSAL_STATE;

@Stateless
public class ProposalManager implements ProposalManagerLocal {

	@Inject
	private EntityManager em;
	
	@Override
	public Proposal updateProposal(long id, String name, PROPOSAL_STATE state, String description) {
		Proposal proposal = getProposal(id);
		proposal.setName(name);
		proposal.setState(state);
		proposal.setDescription(description);
		proposal.setLastUpdate(new Date());
		em.merge(proposal);
		return proposal;
	}
	
	@Override
	public Proposal updateProposal(long id, String name, PROPOSAL_STATE state) {
		Proposal proposal = getProposal(id);
		proposal.setName(name);
		proposal.setState(state);
		proposal.setLastUpdate(new Date());
		em.merge(proposal);
		return proposal;
	}
	
	@Override
	public Proposal updateProposal(long id, String description) {
		Proposal proposal = getProposal(id);
		proposal.setDescription(description);
		proposal.setLastUpdate(new Date());
		em.merge(proposal);
		return proposal;
	}

	@Override
	public Proposal addProposal(String name, String description) {
		Proposal proposal = new Proposal(name, description);
		proposal.setLastUpdate(new Date());
		em.persist(proposal);
		return proposal;
	}

	@Override
	public Proposal getProposal(long id) {
		return em.find(Proposal.class, id);
	}

	@Override
	public List<Proposal> getAllProposalsWithRange(int range, boolean filteredByNameAndState) {
		TypedQuery<Proposal> query;
		if(filteredByNameAndState)
			query = em.createNamedQuery(FIND_AMOUNT_ORDER_BY_STATE_AND_NAME_QUERY_NAME, Proposal.class);
		else
			query = em.createNamedQuery(FIND_ALL_QUERY_NAME, Proposal.class);
		return query.setMaxResults(range).getResultList();
	}
}
