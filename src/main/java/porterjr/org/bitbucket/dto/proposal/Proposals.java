package porterjr.org.bitbucket.dto.proposal;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;


@XmlRootElement
@XmlSeeAlso({Proposal.class})
public class Proposals extends ArrayList<Proposal> {
	private static final long serialVersionUID = 5773303127310329987L;

	public Proposals() {
	}
	
	public Proposals(List<Proposal> proposals) {
		setProposals(proposals);
	}
	
	@XmlElement(name = "proposal")
	List<Proposal> getProposals() {
		return this;
	}
	
	public void setProposals(List<Proposal> proposals) {
		this.addAll(proposals);
	}
	
}
