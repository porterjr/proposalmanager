package porterjr.org.bitbucket.dto.proposal;
import static porterjr.org.bitbucket.dto.proposal.constant.ProposalQuery.FIND_ALL_QUERY_CONTENT;
import static porterjr.org.bitbucket.dto.proposal.constant.ProposalQuery.FIND_ALL_QUERY_NAME;
import static porterjr.org.bitbucket.dto.proposal.constant.ProposalQuery.FIND_AMOUNT_ORDER_BY_STATE_AND_NAME_QUERY_CONTENT;
import static porterjr.org.bitbucket.dto.proposal.constant.ProposalQuery.FIND_AMOUNT_ORDER_BY_STATE_AND_NAME_QUERY_NAME;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import porterjr.org.bitbucket.dto.proposal.constant.PROPOSAL_STATE;

@Entity
@Access(AccessType.FIELD)
@NamedQueries({
@NamedQuery(name=FIND_ALL_QUERY_NAME, query=FIND_ALL_QUERY_CONTENT),
@NamedQuery(name=FIND_AMOUNT_ORDER_BY_STATE_AND_NAME_QUERY_NAME, query=FIND_AMOUNT_ORDER_BY_STATE_AND_NAME_QUERY_CONTENT)})
@XmlRootElement
//order in XML element
@XmlType(propOrder={"id","name","state","description","lastUpdate"})
public class Proposal {

	public Proposal() {
	}
	
	public Proposal(String name, String description) {
		this.name = name;
		this.description = description;
		this.state = PROPOSAL_STATE.CREATED;
	}
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_seq_gen")
	@SequenceGenerator(name = "users_seq_gen", sequenceName = "proposal_id_seq", allocationSize=1)
	private long id;
	
	@Enumerated(EnumType.STRING)
	@NotNull
	private PROPOSAL_STATE state;
	
	@NotNull
	private String description;
	
	@NotNull
	private String name;
	//TODO check format of date if correct
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdate;
	
	@OneToOne(mappedBy="proposal")
	private ProposalHistory proposalHistory;
	
	public long getId() {
		return this.id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public PROPOSAL_STATE getState() {
		return this.state;
	}
	
	public void setState(PROPOSAL_STATE state) {
		this.state = state;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Date getLastUpdate() {
		return this.lastUpdate;
	}
	
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
}
