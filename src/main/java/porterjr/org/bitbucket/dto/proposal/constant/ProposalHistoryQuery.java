package porterjr.org.bitbucket.dto.proposal.constant;

public interface ProposalHistoryQuery {
	final String FIND_PROPOSAL_HISTORY_QUERY_NAME = "ProposalHistory.findProposalHistory";
	final String FIND_PROPOSAL_HISTORY_QUERY_CONTENT ="SELECT p FROM ProposalHistory p WHERE p.proposal.id=:id_proposal order by p.updateTime desc";
}
