package porterjr.org.bitbucket.dto.proposal.constant;

public interface ProposalQuery {
	final String FIND_ALL_QUERY_NAME = "Proposal.findAll";
	final String FIND_ALL_QUERY_CONTENT ="SELECT p FROM Proposal p";
	final String FIND_AMOUNT_ORDER_BY_STATE_AND_NAME_QUERY_NAME = "Proposal.findAmountOrderedByStateAndName";
	final String FIND_AMOUNT_ORDER_BY_STATE_AND_NAME_QUERY_CONTENT = "SELECT p FROM Proposal p ORDER BY p.name, p.state";
}
