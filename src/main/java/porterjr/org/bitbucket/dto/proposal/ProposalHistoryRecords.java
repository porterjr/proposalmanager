package porterjr.org.bitbucket.dto.proposal;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

@XmlRootElement
@XmlSeeAlso({ProposalHistory.class})
public class ProposalHistoryRecords extends ArrayList<ProposalHistory> {
	
	private static final long serialVersionUID = -933056776683975547L;

	public ProposalHistoryRecords() {
	}
	
	public ProposalHistoryRecords(List<ProposalHistory> proposalHistoryRecords) {
		setProposalHistoryRecords(proposalHistoryRecords);
	}
	
	@XmlElement(name = "Proposal")
	public List<ProposalHistory> getProposals() {
		return this;
	}
	
	public void setProposalHistoryRecords(List<ProposalHistory> proposalHistoryRecords) {
		this.addAll(proposalHistoryRecords);
	}
}
