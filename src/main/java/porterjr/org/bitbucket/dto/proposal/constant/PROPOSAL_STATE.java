package porterjr.org.bitbucket.dto.proposal.constant;

/**
 * descibes state of proposal
 * @author pg
 *
 */
public enum PROPOSAL_STATE {
	
	CREATED, DELETED, VERIFIED, REJECTED, ACCEPTED, PUBLISHED,NONE;
}
