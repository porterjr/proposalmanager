package porterjr.org.bitbucket.dto.proposal;

import static porterjr.org.bitbucket.dto.proposal.constant.ProposalHistoryQuery.FIND_PROPOSAL_HISTORY_QUERY_CONTENT;
import static porterjr.org.bitbucket.dto.proposal.constant.ProposalHistoryQuery.FIND_PROPOSAL_HISTORY_QUERY_NAME;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import porterjr.org.bitbucket.dto.proposal.constant.PROPOSAL_STATE;

@Entity
@Access(AccessType.FIELD)
@NamedQuery(name=FIND_PROPOSAL_HISTORY_QUERY_NAME, query=FIND_PROPOSAL_HISTORY_QUERY_CONTENT)
@XmlRootElement
@XmlType(propOrder={"state","description","updateTime","reason"})
@Table(name="history")
public class ProposalHistory  {
	
	public ProposalHistory() {

	}
	
	public ProposalHistory(Proposal proposal, PROPOSAL_STATE state) {
		this.description = proposal.getDescription();
		this.state = state;
		this.proposal = proposal;
		this.updateTime = new Date();
	}
	
	public ProposalHistory(Proposal proposal) {
		this.description = proposal.getDescription();
		this.state = proposal.getState();
		this.proposal = proposal;
		this.updateTime = new Date();
	}
	
	public ProposalHistory(Proposal proposal, PROPOSAL_STATE state, String reason) {
		this.description = proposal.getDescription();
		this.state = state;
		this.proposal = proposal;
		this.updateTime = new Date();
		this.reason = reason;
	}
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "historyProposal_seq_gen")
	@SequenceGenerator(name = "historyProposal_seq_gen", sequenceName = "history_id_seq", allocationSize=1)
	private long id;
	
	@OneToOne
	@JoinColumn(name="id_proposal")
	private Proposal proposal;
	
	@Enumerated(EnumType.STRING)
	@NotNull
	private PROPOSAL_STATE state;
	
	@NotNull
	private String description;
	
	//TODO check format of date if correct
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateTime;
	
	private String reason;

	//exclude that filed during presentation of ProposalHistory using jaxb
	@XmlTransient
	public long getId() {
		return this.id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public PROPOSAL_STATE getState() {
		return this.state;
	}
	
	public void setState(PROPOSAL_STATE state) {
		this.state = state;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Date getUpdateTime() {
		return this.updateTime;
	}
	
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
	public String getReason() {
		return this.reason;
	}
	
	public void setReason(String reason) {
		this.reason = reason;
	}
}
