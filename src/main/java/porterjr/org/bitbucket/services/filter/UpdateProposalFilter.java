package porterjr.org.bitbucket.services.filter;

import javax.ejb.EJB;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;

import porterjr.org.bitbucket.dao.busniess.UpdateRequestValidatorLocal;
import porterjr.org.bitbucket.dao.busniess.UriWrapper;
import porterjr.org.bitbucket.dao.busniess.UriWrapperBuilderLocal;


@Provider
public class UpdateProposalFilter implements ContainerRequestFilter {

	// check if we can update from one state to other for particular Proposal
	@EJB
	private UpdateRequestValidatorLocal requestValidator;

	@EJB
	private UriWrapperBuilderLocal uriWrapperBuilder;

	@Override
	public void filter(ContainerRequestContext requestContext) {
		UriInfo uriInfo = requestContext.getUriInfo();
		UriWrapper wrapper = uriWrapperBuilder.buildUriWrapper(uriInfo.getRequestUri());
		if (!requestValidator.isRequestValid(wrapper)) {
			Response response = Response.status(Response.Status.FORBIDDEN)
					.entity("Update to that proposal state forbidden (check available proposal state transitions) or trying to update to the state which proposal actually has")
					.type(MediaType.TEXT_PLAIN).build();
			throw new WebApplicationException(response);
		}
	}
}