package porterjr.org.bitbucket.services;


import static porterjr.org.bitbucket.services.constant.CreateProposalPaths.ADD_PROPOSAL;
import static porterjr.org.bitbucket.services.constant.CreateProposalPaths.SERVICE;

import javax.ejb.EJB;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import porterjr.org.bitbucket.dao.ProposalManagerLocal;
import porterjr.org.bitbucket.dto.proposal.Proposal;
import porterjr.org.bitbucket.dto.proposal.ProposalHistory;

@Path(SERVICE)
public class CreateProposal {
	
	@EJB
	private ProposalManagerLocal proposalManager;
	
	@Inject
	private Event<ProposalHistory> proposalHistoryEvent;
	
	@Path("/"+ADD_PROPOSAL)
	@POST
	public Response addProposal(@NotNull @QueryParam("name") String proposalName,@NotNull  @QueryParam("description") String proposalDescription) {
		Proposal proposal = proposalManager.addProposal(proposalName, proposalDescription);
		proposalHistoryEvent.fire(new ProposalHistory(proposal));
		return Response.ok(proposal).build();
	}
}
