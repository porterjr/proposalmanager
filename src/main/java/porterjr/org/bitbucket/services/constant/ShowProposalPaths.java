package porterjr.org.bitbucket.services.constant;

public interface ShowProposalPaths {
	final static String GET_PROPOSALS ="getProposals";
	final static String GET_PROPOSAL ="getProposal";
	final static String GET_PROPOSAL_HISTORY ="getProposalHistory";
	final static String SERVICE ="showProposal";
}
