package porterjr.org.bitbucket.services.constant;

public interface CreateProposalPaths {

	final static String ADD_PROPOSAL = "addProposal";
	final static String SERVICE = "createProposal";
	
}
