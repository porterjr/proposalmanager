package porterjr.org.bitbucket.services.constant;

public interface UpdateProposalPaths {
	final static String PUBLISH_PROPOSAL = "publishProposal";
	final static String VERIFY_PROPOSAL = "verifyProposal";
	final static String REMOVE_PROPOSAL = "removeProposal";
	final static String REJECT_PROPOSAL = "rejectProposal";
	final static String ACCEPT_PROPOSAL = "acceptProposal";
	final static String EDIT_DESCRIPTION = "editDescription";
	final static String SERVICE = "updateProposal";
	final static String FULL_REQUEST = "/proposalManager/rest/updateProposal";
	final static String EDIT_DESCRIPTION_REQUEST = "/proposalManager/rest/updateProposal/"+EDIT_DESCRIPTION;
}
