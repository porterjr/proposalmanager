package porterjr.org.bitbucket.services;

import static porterjr.org.bitbucket.services.constant.UpdateProposalPaths.*;

import javax.ejb.EJB;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import porterjr.org.bitbucket.dao.ProposalManagerLocal;
import porterjr.org.bitbucket.dto.proposal.Proposal;
import porterjr.org.bitbucket.dto.proposal.ProposalHistory;
import porterjr.org.bitbucket.dto.proposal.constant.PROPOSAL_STATE;

@Path(SERVICE)
public class UpdateProposal {
	@EJB
	private ProposalManagerLocal proposalManager;
	
	@Inject
	private Event<ProposalHistory> proposalHistoryEvent;
	
	@PUT
	@Path("/"+REMOVE_PROPOSAL)
	public Response removeProposal(@NotNull @QueryParam("id") long proposalId,@NotNull @QueryParam("reason") String reason) {
		Proposal proposal = updateProposalStateBasedOnProposalId(PROPOSAL_STATE.DELETED, proposalId,reason);
		return Response.ok(proposal).build();
	}
	
	@PUT
	@Path("/"+VERIFY_PROPOSAL)
	public Response verifyProposal(@NotNull @QueryParam("id") long proposalId) {
		Proposal proposal = updateProposalStateBasedOnProposalId(PROPOSAL_STATE.VERIFIED, proposalId);
		return Response.ok(proposal).build();
	}
	
	@PUT
	@Path("/"+REJECT_PROPOSAL)
	public Response rejectProposal(@NotNull @QueryParam("id") long proposalId,@NotNull @QueryParam("reason") String reason) {
		Proposal proposal = updateProposalStateBasedOnProposalId(PROPOSAL_STATE.REJECTED, proposalId, reason);
		return Response.ok(proposal).build();
	}
	
	@PUT
	@Path("/"+ACCEPT_PROPOSAL)
	public Response acceptProposal(@NotNull @QueryParam("id") long proposalId) {
		Proposal proposal = updateProposalStateBasedOnProposalId(PROPOSAL_STATE.ACCEPTED, proposalId);
		return Response.ok(proposal).build();
	}
	
	@PUT
	@Path("/"+PUBLISH_PROPOSAL)
	public Response publishProposal(@NotNull @QueryParam("id") long proposalId) {
		Proposal proposal = updateProposalStateBasedOnProposalId(PROPOSAL_STATE.PUBLISHED, proposalId);
		return Response.ok(proposal).build();
	}
	
	@PUT
	@Path("/"+EDIT_DESCRIPTION)
	public Response editDesciption(@NotNull @QueryParam("id") long proposalId,@NotNull @QueryParam("description") String description) {
		Proposal proposal = updateProposalDescriptionBasedOnProposalId(proposalId,description);
		return Response.ok(proposal).build();
	}
	
	private Proposal updateProposalStateBasedOnProposalId(PROPOSAL_STATE state, long proposalId) {
		Proposal proposalToUpdate = proposalManager.getProposal(proposalId);
		proposalHistoryEvent.fire(new ProposalHistory(proposalToUpdate,state));
		return proposalManager.updateProposal(proposalId, proposalToUpdate.getName(), state);
	}
	
	private Proposal updateProposalStateBasedOnProposalId(PROPOSAL_STATE state, long proposalId, String reason) {
		Proposal proposalToUpdate = proposalManager.getProposal(proposalId);
		proposalHistoryEvent.fire(new ProposalHistory(proposalToUpdate,state,reason));
		return proposalManager.updateProposal(proposalId, proposalToUpdate.getName(), state);
	}
	
	private Proposal updateProposalDescriptionBasedOnProposalId(long proposalId, String descriprion) {
		Proposal proposalToUpdate = proposalManager.getProposal(proposalId);
		ProposalHistory proposalHistory = new ProposalHistory(proposalToUpdate);
		proposalHistory.setDescription(descriprion);
		proposalHistoryEvent.fire(proposalHistory);
		return proposalManager.updateProposal(proposalId, descriprion);
	}
}
