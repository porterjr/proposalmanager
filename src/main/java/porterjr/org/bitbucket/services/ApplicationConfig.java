package porterjr.org.bitbucket.services;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import porterjr.org.bitbucket.services.filter.UpdateProposalFilter;


@ApplicationPath("rest")
public class ApplicationConfig extends Application {

	private final Set<Class<?>> classes;

	public ApplicationConfig() {
		final HashSet<Class<?>> set = new HashSet<>();
		set.add(CreateProposal.class);
		set.add(ShowProposal.class);
		set.add(UpdateProposal.class);
		set.add(UpdateProposalFilter.class);
		classes = Collections.unmodifiableSet(set);
	}

	@Override
	public Set<Class<?>> getClasses() {
		return classes;
	}
}
