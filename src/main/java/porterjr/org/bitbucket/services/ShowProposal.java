package porterjr.org.bitbucket.services;

import static porterjr.org.bitbucket.services.constant.ShowProposalPaths.GET_PROPOSAL;
import static porterjr.org.bitbucket.services.constant.ShowProposalPaths.GET_PROPOSALS;
import static porterjr.org.bitbucket.services.constant.ShowProposalPaths.GET_PROPOSAL_HISTORY;
import static porterjr.org.bitbucket.services.constant.ShowProposalPaths.SERVICE;

import java.util.List;

import javax.ejb.EJB;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import porterjr.org.bitbucket.dao.ProposalHistoryManagerLocal;
import porterjr.org.bitbucket.dao.ProposalManagerLocal;
import porterjr.org.bitbucket.dto.proposal.Proposal;
import porterjr.org.bitbucket.dto.proposal.ProposalHistoryRecords;

@Path(SERVICE)
public class ShowProposal {

	@EJB
	private ProposalManagerLocal proposalManager;
	
	@EJB
	private ProposalHistoryManagerLocal proposalHistoryManager;
	
	/**
	 * 
	 * @param range how many records should be returned
	 * @param filterByNameAndState should returned records filtered by name and state
	 * @return proposals
	 */
	@GET
	@Path("/"+GET_PROPOSALS)
	public Response getProposals(@DefaultValue("10") @QueryParam("range") int range, @DefaultValue("false") @QueryParam("filterByNameAndState") boolean filterByNameAndState) {
		List<Proposal> proposals = proposalManager.getAllProposalsWithRange(range,filterByNameAndState);
		GenericEntity<List<Proposal>> entity = new GenericEntity<List<Proposal>>(proposals) {};
		return Response.ok(entity).build();
	}
	
	@GET
	@Path("/"+GET_PROPOSAL)
	public Response getProposal(@NotNull @QueryParam("id") long id) {
		Proposal proposal = proposalManager.getProposal(id);
		return Response.ok(proposal).build();
	}
	
	@GET
	@Path("/"+GET_PROPOSAL_HISTORY)
	public Response getProposalHistory(@NotNull @QueryParam("id") long id) {
		ProposalHistoryRecords proposalHistory = proposalHistoryManager.getProposalHistory(id);
		return Response.ok(proposalHistory).build();
	}
}
