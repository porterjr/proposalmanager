CREATE ROLE porterjr LOGIN
  ENCRYPTED PASSWORD 'asd123'
  SUPERUSER INHERIT CREATEDB CREATEROLE NOREPLICATION;




-- Table: Proposal

DROP TABLE IF EXISTS Proposal;

CREATE TABLE Proposal
(
  id serial NOT NULL,
  name text NOT NULL,
  description text NOT NULL,
  state text NOT NULL,
  lastUpdate timestamp without time zone NOT NULL,
  CONSTRAINT proposal_id PRIMARY KEY (id )
)
WITH (
  OIDS=FALSE
);
ALTER TABLE Proposal
  OWNER TO porterjr;


-- Table: History

DROP TABLE IF EXISTS History;

CREATE TABLE History
(
  id serial NOT NULL,
  id_proposal serial NOT NULL,
  state text NOT NULL,
  changeStateTimestamp timestamp without time zone NOT NULL,
  reason text,
  description text NOT NULL,
  CONSTRAINT history_id PRIMARY KEY (id ),
  CONSTRAINT proposal_id FOREIGN KEY (id_proposal)
      REFERENCES Proposal (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE History
  OWNER TO porterjr;

