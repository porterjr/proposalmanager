package porterjr.org.bitbucket.services;

import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import porterjr.org.bitbucket.services.filter.UpdateProposalFilter;

import static junitparams.JUnitParamsRunner.$;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class ApplicationConfigTest {

	private ApplicationConfig sut = new ApplicationConfig();
	
	@Test
	public void applicationConfigShouldContainsFourClassesRegistered() {
		assertThat(sut.getClasses().size(),is(4));
	}
	
	@Test
	@Parameters(method="parametersForApplicationConfigShouldContainSpecificClass")
	public void applicationConfigShouldContainSpecificClass(Class<?> clazz) {
		assertThat(sut.getClasses().contains(clazz), is(true));
	}
	
	public Object[] parametersForApplicationConfigShouldContainSpecificClass() {
	        return $(
	        		$(CreateProposal.class),
	        		$(ShowProposal.class),
	        		$(UpdateProposal.class),
	        		$(UpdateProposalFilter.class)
	        );
	    }
}
