package porterjr.org.bitbucket.services;

import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import porterjr.org.bitbucket.dao.ProposalHistoryManagerLocal;
import porterjr.org.bitbucket.dao.ProposalManagerLocal;

@RunWith(MockitoJUnitRunner.class)
public class ShowProposalTest {

	@InjectMocks
	private ShowProposal sut;

	@Mock
	private ProposalManagerLocal proposalManager;
	
	@Mock
	private ProposalHistoryManagerLocal proposalHistoryManager;

	private final static long ID = 2;
	private final static int RANGE = 1;

	@Test
	public void getProposalShouldInvokeGetProposalFromManager() {
		sut.getProposal(ID);

		verify(proposalManager).getProposal(ID);
	}
	
	@Test
	public void getProposalsNotFilteredByStateAndNameShouldInvokeGetAllProposalsFromManager() {
		sut.getProposals(RANGE, false);
		
		verify(proposalManager).getAllProposalsWithRange(RANGE, false);
	}
	
	@Test
	public void getProposalsFilteredByStateAndNameShouldInvokeGetAllProposalsFromManager() {
		sut.getProposals(RANGE, true);

		verify(proposalManager).getAllProposalsWithRange(RANGE, true);
	}

	@Test
	public void getProposalHistoryShouldInvokeGetProposalHistoryFromHistoryManager() {
		sut.getProposalHistory(ID);
		
		verify(proposalHistoryManager).getProposalHistory(ID);
	}
}
