package porterjr.org.bitbucket.services;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.enterprise.event.Event;
import javax.ws.rs.core.Response;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import porterjr.org.bitbucket.dao.ProposalManagerLocal;
import porterjr.org.bitbucket.dto.proposal.Proposal;
import porterjr.org.bitbucket.dto.proposal.ProposalHistory;
import porterjr.org.bitbucket.dto.proposal.constant.PROPOSAL_STATE;

@RunWith(MockitoJUnitRunner.class)
public class UpdateProposalTest {

	@InjectMocks
	private UpdateProposal sut;

	@Mock
	private ProposalManagerLocal proposalManager;

	@Mock
	private Event<ProposalHistory> event;
	
	private final static String NAME = "name";
	private final static String DESCRIPTION = "description";
	private final static long ID = 2;
	private final static String REASON = "some reason of rejection/remove";
	private Proposal proposal = new Proposal(NAME, DESCRIPTION);

	@Before
	public void init() {
		when(proposalManager.getProposal(ID)).thenReturn(proposal);
	}
	
	@After
	public void afterEveryUpdateActionHistoryProposalEventShouldBeTriggered() {
		verify(event).fire(Mockito.any(ProposalHistory.class));
	}

	@Test
	public void removeProposalShouldTriggerUpdateProposalFromProposalManagerRemote() {
		sut.removeProposal(ID,REASON);
		verify(proposalManager).updateProposal(ID, NAME, PROPOSAL_STATE.DELETED);
	}

	@Test
	public void removeProposalShouldReturnResponseOk() {
		Response res = sut.removeProposal(ID,REASON);
		Response okResponse = Response.ok().build();
		assertThat(res.getStatus(), is(okResponse.getStatus()));
	}
	
	@Test
	public void verifyProposalShouldTriggerUpdateProposalFromProposalManagerRemote() {
		sut.verifyProposal(ID);
		verify(proposalManager).updateProposal(ID, NAME, PROPOSAL_STATE.VERIFIED);
	}
	
	@Test
	public void verifyProposalShouldReturnResponseOk() {
		Response res = sut.verifyProposal(ID);
		Response okResponse = Response.ok().build();
		assertThat(res.getStatus(), is(okResponse.getStatus()));
	}
	
	@Test
	public void acceptProposalShouldTriggerUpdateProposalFromProposalManagerRemote() {
		sut.acceptProposal(ID);
		verify(proposalManager).updateProposal(ID, NAME, PROPOSAL_STATE.ACCEPTED);
	}
	
	@Test
	public void acceptProposalShouldReturnResponseOk() {
		Response res = sut.acceptProposal(ID);
		Response okResponse = Response.ok().build();
		assertThat(res.getStatus(), is(okResponse.getStatus()));
	}
	
	@Test
	public void publishProposalShouldTriggerUpdateProposalFromProposalManagerRemote() {
		sut.publishProposal(ID);
		verify(proposalManager).updateProposal(ID, NAME, PROPOSAL_STATE.PUBLISHED);
	}
	
	@Test
	public void publishProposalShouldReturnResponseOk() {
		Response res = sut.publishProposal(ID);
		Response okResponse = Response.ok().build();
		assertThat(res.getStatus(), is(okResponse.getStatus()));
	}
	
	@Test
	public void rejectProposalShouldTriggerUpdateProposalFromProposalManagerRemote() {
		sut.rejectProposal(ID,REASON);
		verify(proposalManager).updateProposal(ID, NAME, PROPOSAL_STATE.REJECTED);
	}
	
	@Test
	public void rejectProposalShouldReturnResponseOk() {
		Response res = sut.rejectProposal(ID,REASON);
		Response okResponse = Response.ok().build();
		assertThat(res.getStatus(), is(okResponse.getStatus()));
	}
	
	@Test
	public void editDescriptionShouldTriggerUpdateProposal() {
		sut.editDesciption(ID, DESCRIPTION);
		verify(proposalManager).updateProposal(ID, DESCRIPTION);
	}
	
	@Test
	public void editDescriptionShouldReturnResponseOk() {
		Response res =sut.editDesciption(ID, DESCRIPTION);
		Response okResponse = Response.ok().build();
		assertThat(res.getStatus(), is(okResponse.getStatus()));
	}
}
