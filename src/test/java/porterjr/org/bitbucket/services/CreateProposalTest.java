package porterjr.org.bitbucket.services;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import javax.enterprise.event.Event;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import porterjr.org.bitbucket.dao.ProposalManagerLocal;
import porterjr.org.bitbucket.dto.proposal.Proposal;
import porterjr.org.bitbucket.dto.proposal.ProposalHistory;

@RunWith(MockitoJUnitRunner.class)
public class CreateProposalTest {

	@InjectMocks
	private CreateProposal sut;

	@Mock
	private ProposalManagerLocal proposalManager;

	@Mock
	private Event<ProposalHistory> event;
	
	@Mock
	private Proposal proposal;
	
	private final static String NAME = "name";
	private final static String DESCRIPTION = "description";

	@Before
	public void init() {
		when(proposalManager.addProposal(NAME, DESCRIPTION)).thenReturn(proposal);
	}
	
	@Test
	public void addProposalShouldTriggerAddProposalFromProposalManagerRemote() {
		sut.addProposal(NAME, DESCRIPTION);
		verify(proposalManager).addProposal(NAME, DESCRIPTION);
	}

	@Test
	public void addProposalShouldReturnResponseOk() {
		sut.addProposal(NAME, DESCRIPTION);
		Response okResponse = Response.ok().build();
		assertThat(sut.addProposal(NAME, DESCRIPTION).getStatus(), is(okResponse.getStatus()));
	}
	
	@Test 
	public void addProposalShouldFireProposalHistoryEvent() {
		sut.addProposal(NAME, DESCRIPTION);
		verify(event).fire(Mockito.any(ProposalHistory.class));
	}
}
