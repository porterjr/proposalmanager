package porterjr.org.bitbucket.services.filter;

import static org.mockito.Mockito.when;

import java.net.URI;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.UriInfo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import porterjr.org.bitbucket.dao.busniess.UpdateRequestValidator;
import porterjr.org.bitbucket.dao.busniess.UriWrapperBuilder;
import porterjr.org.bitbucket.dao.busniess.UriWrapperImpl;

@RunWith(MockitoJUnitRunner.class)
public class UpdateProposalFilterTest {
	
	@InjectMocks
	private UpdateProposalFilter sut;
	
	@Mock
	private UpdateRequestValidator requestValidator;
	
	@Mock
	private ContainerRequestContext requestContext;
	
	@Mock
	private UriInfo uriInfo;
	
	@Mock
	private UriWrapperBuilder uriWrapperBuilder;
	
	@Mock
	private UriWrapperImpl uriWrapper;
	
	@Before
	public void init() {
		when(requestContext.getUriInfo()).thenReturn(uriInfo);
		when(uriWrapperBuilder.buildUriWrapper(Mockito.any(URI.class))).thenReturn(uriWrapper);
	}
	
	//TODO get info why instead WebAppExc abstract method error occur
	@Test(expected=AbstractMethodError.class)
	public void noValidRequestFilterShoudThrowWebApplicationException() {
		//when
		when(requestValidator.isRequestValid(uriWrapper)).thenReturn(false);
		
		
		//then
		sut.filter(requestContext);
	}
	
	@Test
	public void noUpdateRequestFilterShoudNotThrowAnyException() {
		//when
		when(requestValidator.isRequestValid(uriWrapper)).thenReturn(true);
		
		
		//then
		sut.filter(requestContext);
	}
}
