package porterjr.org.bitbucket.dao;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import porterjr.org.bitbucket.dao.factory.EntityFactory;
import porterjr.org.bitbucket.dto.proposal.Proposal;
import porterjr.org.bitbucket.dto.proposal.constant.PROPOSAL_STATE;

@RunWith(Arquillian.class)
public class ProposalManagerIT {
	private final static String BEANS_XML = "beans.xml";
	private final static String PERSISTENCE_XML = "persistence.xml";
	private final static String TEST_PERSISTENCE_XML = "test-persistence.xml";
	private final static String RANDOM_NAME = "randomName";
	private final static String OTHER_RANDOM_NAME = "otherRandomName";
	private final static String SOME_DESCRIPTION = "some description";
	private final static String OTHER_DESCRIPTION = "other description";
	private final static String A_NAME = "A";
	private final static String B_NAME = "B";
	private final static String C_NAME = "C";
	private final static String FIND_ALL_QUERY_NAME = "SELECT * FROM Proposal";
	private final static String DELETE_QUERY_NAME = "DELETE FROM Proposal";
	
	@Deployment
	public static JavaArchive createDeployment() {

		JavaArchive jar = ShrinkWrap.create(JavaArchive.class).addPackage(ProposalManagerLocal.class.getPackage())
				.addClass(EntityFactory.class)
				.addPackage(Proposal.class.getPackage()).addAsManifestResource(TEST_PERSISTENCE_XML, PERSISTENCE_XML)
				.addAsManifestResource(EmptyAsset.INSTANCE, BEANS_XML);
		return jar;
	}
	
	@EJB
	private ProposalManagerLocal proposalManager;
	
	@Inject
	private EntityManager em;
	
	@Inject
	UserTransaction ut;

	@Before
	public void removeAllFromDb() throws Exception {
		ut.begin();
		em.createNativeQuery(DELETE_QUERY_NAME).executeUpdate();
		ut.commit();
	}
	
	@Test
	public void proposalManagerShouldExists() {
		assertThat(proposalManager,is(notNullValue()));
	}
	
	@Test
	public void afterAddingOneProposalThereShouldBeOneInDbWithStateCreated() {
		proposalManager.addProposal(RANDOM_NAME, SOME_DESCRIPTION);
		List<Proposal> proposals = getAllProposals();
		Proposal proposal = proposals.get(0);
		assertThat(proposals.size(),is(1));
		assertThat(proposal.getName(),is(RANDOM_NAME));
		assertThat(proposal.getDescription(),is(SOME_DESCRIPTION));
		assertThat(proposal.getState(),is(PROPOSAL_STATE.CREATED));
	}
	
	@Test
	public void updateProposalShouldUpdateProposalOnDbSide() {
		proposalManager.addProposal(RANDOM_NAME, SOME_DESCRIPTION);
		List<Proposal> proposals = getAllProposals();
		Proposal proposal = proposals.get(0);
		proposal.setName(OTHER_RANDOM_NAME);
		proposal.setDescription(OTHER_DESCRIPTION);
		proposal.setState(PROPOSAL_STATE.DELETED);
		proposalManager.updateProposal(proposal.getId(), proposal.getName(), proposal.getState(), proposal.getDescription());
		proposals = getAllProposals();
		proposal = proposals.get(0);
		assertThat(proposals.size(),is(1));
		assertThat(proposal.getName(),is(OTHER_RANDOM_NAME));
		assertThat(proposal.getDescription(),is(OTHER_DESCRIPTION));
		assertThat(proposal.getState(),is(PROPOSAL_STATE.DELETED));
	}
	
	@Test
	public void getAllProposalsWithRangeShouldGivesProperListWithSpecificAmountOfElements() {
		proposalManager.addProposal(C_NAME, SOME_DESCRIPTION);
		proposalManager.addProposal(B_NAME, SOME_DESCRIPTION);
		proposalManager.addProposal(A_NAME, SOME_DESCRIPTION);
		List<Proposal> proposals = proposalManager.getAllProposalsWithRange(2, false);
		assertThat(proposals.size(),is(2));
		Proposal firstProposal = proposals.get(0);
		Proposal secondProposal = proposals.get(1);
		assertThat(firstProposal,is(notNullValue()));
		assertThat(firstProposal.getName(),is(C_NAME));
		assertThat(firstProposal.getState(),is(PROPOSAL_STATE.CREATED));
		assertThat(secondProposal,is(notNullValue()));
		assertThat(secondProposal.getName(),is(B_NAME));
		assertThat(secondProposal.getState(),is(PROPOSAL_STATE.CREATED));
	}
	
	@Test
	public void getAllProposalsOrderedByNameAndStateWithRangeShouldGivesProperOrderedListWithSpecificAmountOfElements() {
		proposalManager.addProposal(C_NAME, SOME_DESCRIPTION);
		proposalManager.addProposal(B_NAME, SOME_DESCRIPTION);
		proposalManager.addProposal(A_NAME, SOME_DESCRIPTION);
		List<Proposal> proposals = proposalManager.getAllProposalsWithRange(2, true);
		assertThat(proposals.size(),is(2));
		Proposal firstProposal = proposals.get(0);
		Proposal secondProposal = proposals.get(1);
		assertThat(firstProposal,is(notNullValue()));
		assertThat(firstProposal.getName(),is(A_NAME));
		assertThat(firstProposal.getState(),is(PROPOSAL_STATE.CREATED));
		assertThat(secondProposal,is(notNullValue()));
		assertThat(secondProposal.getName(),is(B_NAME));
		assertThat(secondProposal.getState(),is(PROPOSAL_STATE.CREATED));
	}
	
	@Test
	public void getAllProposalsWithRangeBiggerThenAmountOfElementsShouldReturnMaxElementAmount() {
		proposalManager.addProposal(C_NAME, SOME_DESCRIPTION);
		proposalManager.addProposal(B_NAME, SOME_DESCRIPTION);
		proposalManager.addProposal(A_NAME, SOME_DESCRIPTION);
		List<Proposal> proposals = proposalManager.getAllProposalsWithRange(12, true);
		assertThat(proposals.size(),is(3));
	}
	
	@SuppressWarnings("unchecked")
	private List<Proposal> getAllProposals() {
		return em.createNativeQuery(FIND_ALL_QUERY_NAME,Proposal.class).getResultList();
	}
	
}
