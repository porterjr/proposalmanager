package porterjr.org.bitbucket.dao.business;

import org.junit.Before;
import org.junit.Test;

import porterjr.org.bitbucket.dao.busniess.ProposalStateValidator;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static porterjr.org.bitbucket.dto.proposal.constant.PROPOSAL_STATE.*;

public class ProposalStateValidatorTest {
	
	private ProposalStateValidator sut = new ProposalStateValidator();
	
	@Before
	public void init() {
		sut.init(); 
	}
	
	@Test
	public void createdStateCanTransformToDeletedOrVerified() {
		
		assertThat(sut.isMoveFromBeginningToTargetStatePossible(CREATED, DELETED), is(true));
		assertThat(sut.isMoveFromBeginningToTargetStatePossible(CREATED, VERIFIED), is(true));
	}
	
	@Test
	public void deletedStateCannotTransform(){
		assertThat(sut.isMoveFromBeginningToTargetStatePossible(DELETED, DELETED), is(false));
		assertThat(sut.isMoveFromBeginningToTargetStatePossible(DELETED, VERIFIED), is(false));
		assertThat(sut.isMoveFromBeginningToTargetStatePossible(DELETED, ACCEPTED), is(false));
		assertThat(sut.isMoveFromBeginningToTargetStatePossible(DELETED, REJECTED), is(false));
		assertThat(sut.isMoveFromBeginningToTargetStatePossible(DELETED, PUBLISHED), is(false));
		assertThat(sut.isMoveFromBeginningToTargetStatePossible(DELETED, CREATED), is(false));
	}
	
	@Test 
	public void verifiedStateCanTransformToRejectedOrAccepted() {
		assertThat(sut.isMoveFromBeginningToTargetStatePossible(VERIFIED, REJECTED), is(true));
		assertThat(sut.isMoveFromBeginningToTargetStatePossible(VERIFIED, ACCEPTED), is(true));
	}
	
	@Test 
	public void rejectedStateCannotTransform() {
		assertThat(sut.isMoveFromBeginningToTargetStatePossible(REJECTED, DELETED), is(false));
		assertThat(sut.isMoveFromBeginningToTargetStatePossible(REJECTED, VERIFIED), is(false));
		assertThat(sut.isMoveFromBeginningToTargetStatePossible(REJECTED, ACCEPTED), is(false));
		assertThat(sut.isMoveFromBeginningToTargetStatePossible(REJECTED, REJECTED), is(false));
		assertThat(sut.isMoveFromBeginningToTargetStatePossible(REJECTED, PUBLISHED), is(false));
		assertThat(sut.isMoveFromBeginningToTargetStatePossible(REJECTED, CREATED), is(false));
	}
	
	@Test 
	public void acceptedStateCanTransformToRejectedOrPublished() {
		assertThat(sut.isMoveFromBeginningToTargetStatePossible(ACCEPTED, REJECTED), is(true));
		assertThat(sut.isMoveFromBeginningToTargetStatePossible(ACCEPTED, PUBLISHED), is(true));
	}
	
	@Test 
	public void publishedStateCannotTransform() {
		assertThat(sut.isMoveFromBeginningToTargetStatePossible(PUBLISHED, DELETED), is(false));
		assertThat(sut.isMoveFromBeginningToTargetStatePossible(PUBLISHED, VERIFIED), is(false));
		assertThat(sut.isMoveFromBeginningToTargetStatePossible(PUBLISHED, ACCEPTED), is(false));
		assertThat(sut.isMoveFromBeginningToTargetStatePossible(PUBLISHED, REJECTED), is(false));
		assertThat(sut.isMoveFromBeginningToTargetStatePossible(PUBLISHED, PUBLISHED), is(false));
		assertThat(sut.isMoveFromBeginningToTargetStatePossible(PUBLISHED, CREATED), is(false));
	}
}
