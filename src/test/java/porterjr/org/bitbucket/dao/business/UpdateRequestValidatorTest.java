package porterjr.org.bitbucket.dao.business;

import static junitparams.JUnitParamsRunner.$;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static porterjr.org.bitbucket.services.constant.UpdateProposalPaths.ACCEPT_PROPOSAL;
import static porterjr.org.bitbucket.services.constant.UpdateProposalPaths.PUBLISH_PROPOSAL;
import static porterjr.org.bitbucket.services.constant.UpdateProposalPaths.REJECT_PROPOSAL;
import static porterjr.org.bitbucket.services.constant.UpdateProposalPaths.REMOVE_PROPOSAL;
import static porterjr.org.bitbucket.services.constant.UpdateProposalPaths.VERIFY_PROPOSAL;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import porterjr.org.bitbucket.dao.ProposalManager;
import porterjr.org.bitbucket.dao.busniess.ProposalStateValidator;
import porterjr.org.bitbucket.dao.busniess.UpdateRequestValidator;
import porterjr.org.bitbucket.dao.busniess.UriWrapperImpl;
import porterjr.org.bitbucket.dto.proposal.Proposal;
import porterjr.org.bitbucket.dto.proposal.constant.PROPOSAL_STATE;

@RunWith(JUnitParamsRunner.class)
public class UpdateRequestValidatorTest {

	@InjectMocks
	private UpdateRequestValidator sut;
	
	@Mock
	private ProposalManager proposalManager;
	
	@Mock
	private UriWrapperImpl uriWrapper;
	
	@Mock
	private Proposal proposal;
	
	@Spy
	private ProposalStateValidator proposalStateValidator;
	
	
	@Spy
	private Map<String,PROPOSAL_STATE> mapperRestMethodToState = new HashMap<>();
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		proposalStateValidator.init();
		mapperRestMethodToState.put(PUBLISH_PROPOSAL, PROPOSAL_STATE.PUBLISHED);
		mapperRestMethodToState.put(VERIFY_PROPOSAL, PROPOSAL_STATE.VERIFIED);
		mapperRestMethodToState.put(REMOVE_PROPOSAL, PROPOSAL_STATE.DELETED);
		mapperRestMethodToState.put(REJECT_PROPOSAL, PROPOSAL_STATE.REJECTED);
		mapperRestMethodToState.put(ACCEPT_PROPOSAL, PROPOSAL_STATE.ACCEPTED);
	}
	
	@Test
	@Parameters(method="forNoUpdateMethodRequestShouldBeValid")
	public void forNoUpdateMethodRequestShouldBeValid(String path) {
		//when 
		when(uriWrapper.getPath()).thenReturn(path);
		
		
		//then
		assertThat(sut.isRequestValid(uriWrapper), is(true));
	}
	
	public Object[] forNoUpdateMethodRequestShouldBeValid() {
        return $(
        		$("/proposalManager/rest/createProposal")
        );
    }
	
	@Test
	@Parameters(method="forUpdateMethodCorretnessOfRequestBasedOnProposalState")
	public void forUpdateMethodCorretnessOfRequestBasedOnProposalState(String path, String query, String fullRequestPath, PROPOSAL_STATE actualState, boolean isRequestCorrect) {
		//when 
		when(uriWrapper.getPath()).thenReturn(path);
		when(uriWrapper.getQuery()).thenReturn(query);
		when(uriWrapper.toString()).thenReturn(fullRequestPath);
		when(proposalManager.getProposal(Mockito.anyLong())).thenReturn(proposal);
		when(proposal.getState()).thenReturn(actualState);
		
		
		//then
		assertThat(sut.isRequestValid(uriWrapper), is(isRequestCorrect));
	}
	
	public Object[] forUpdateMethodCorretnessOfRequestBasedOnProposalState() {
        return $(
        		//from CREATED to DELETED or VERIFIED - allowed
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/verifyProposal?id=3",PROPOSAL_STATE.CREATED, true),
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/removeProposal?id=3",PROPOSAL_STATE.CREATED, true),
        		
        		//from NONE to any - forbidden
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/removeProposal?id=3",PROPOSAL_STATE.NONE, false),
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/verifyProposal?id=3",PROPOSAL_STATE.NONE, false),
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/publishProposal?id=3",PROPOSAL_STATE.NONE, false),
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/rejectProposal?id=3",PROPOSAL_STATE.NONE, false),
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/acceptProposal?id=3",PROPOSAL_STATE.NONE, false),
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/editDescription?id=3",PROPOSAL_STATE.NONE, false),
        		
        		//from DELETED to any - forbidden
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/removeProposal?id=3",PROPOSAL_STATE.DELETED, false),
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/verifyProposal?id=3",PROPOSAL_STATE.DELETED, false),
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/publishProposal?id=3",PROPOSAL_STATE.DELETED, false),
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/rejectProposal?id=3",PROPOSAL_STATE.DELETED, false),
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/acceptProposal?id=3",PROPOSAL_STATE.DELETED, false),
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/editDescription?id=3",PROPOSAL_STATE.DELETED, false),
        		
        		//from REJECTED to any - forbidden
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/removeProposal?id=3",PROPOSAL_STATE.REJECTED, false),
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/verifyProposal?id=3",PROPOSAL_STATE.REJECTED, false),
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/publishProposal?id=3",PROPOSAL_STATE.REJECTED, false),
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/rejectProposal?id=3",PROPOSAL_STATE.REJECTED, false),
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/acceptProposal?id=3",PROPOSAL_STATE.REJECTED, false),
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/editDescription?id=3",PROPOSAL_STATE.REJECTED, false),
        		
        		//from PUBLISHED to any - forbidden
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/removeProposal?id=3",PROPOSAL_STATE.PUBLISHED, false),
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/verifyProposal?id=3",PROPOSAL_STATE.PUBLISHED, false),
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/publishProposal?id=3",PROPOSAL_STATE.PUBLISHED, false),
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/rejectProposal?id=3",PROPOSAL_STATE.PUBLISHED, false),
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/acceptProposal?id=3",PROPOSAL_STATE.PUBLISHED, false),
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/editDescription?id=3",PROPOSAL_STATE.PUBLISHED, false),
        		
        		//from VERIFIED to REJECTED,ACCEPTED - allowed
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/rejectProposal?id=3",PROPOSAL_STATE.VERIFIED, true),
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/acceptProposal?id=3",PROPOSAL_STATE.VERIFIED, true),
        		
        		//from ACCEPTED to PUBLISHED, REJECTED
         		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/rejectProposal?id=3",PROPOSAL_STATE.ACCEPTED, true),
        		$("/proposalManager/rest/updateProposal","id=2","http://localhost:8080/proposalManager/rest/updateProposal/publishProposal?id=3",PROPOSAL_STATE.ACCEPTED, true)
        );
    }
}
