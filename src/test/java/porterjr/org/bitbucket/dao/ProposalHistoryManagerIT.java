package porterjr.org.bitbucket.dao;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import porterjr.org.bitbucket.dao.factory.EntityFactory;
import porterjr.org.bitbucket.dto.proposal.Proposal;
import porterjr.org.bitbucket.dto.proposal.ProposalHistory;
import porterjr.org.bitbucket.dto.proposal.ProposalHistoryRecords;

@RunWith(Arquillian.class)
public class ProposalHistoryManagerIT {

	private final static String BEANS_XML = "beans.xml";
	private final static String PERSISTENCE_XML = "persistence.xml";
	private final static String TEST_PERSISTENCE_XML = "test-persistence.xml";
	private final static String RANDOM_NAME = "randomName";
	private final static String OTHER_DESCRIPTION = "other description";
	private final static String DELETE_QUERY_PROPOSAL_NAME = "DELETE FROM Proposal";
	private final static String DELETE_QUERY_HISTORY_NAME = "DELETE FROM History";
	

	@Deployment
	public static JavaArchive createDeployment() {

		JavaArchive jar = ShrinkWrap.create(JavaArchive.class).addPackage(ProposalManagerLocal.class.getPackage())
				.addClass(EntityFactory.class).addPackage(ProposalHistory.class.getPackage())
				.addAsManifestResource(TEST_PERSISTENCE_XML, PERSISTENCE_XML)
				.addAsManifestResource(EmptyAsset.INSTANCE, BEANS_XML);
		return jar;
	}
	
	@EJB
	private ProposalManagerLocal proposalManager;
	
	@EJB
	private ProposalHistoryManagerLocal proposalHistoryManager;
	
	@Inject
	private EntityManager em;
	
	@Inject
	private UserTransaction ut;
	
	@Test
	public void proposalHistoryManagerShouldExists() {
		assertThat(proposalHistoryManager,is(notNullValue()));
	}
	
	@Before
	public void removeAllFromDb() throws Exception {
		ut.begin();
		em.createNativeQuery(DELETE_QUERY_HISTORY_NAME).executeUpdate();
		em.createNativeQuery(DELETE_QUERY_PROPOSAL_NAME).executeUpdate();
		ut.commit();
	}
	
	@Test
	public void afterAddingOneProposalHistoryThereShouldBeOneRecordInDb() {
		Proposal firstProposalToAdd = proposalManager.addProposal(RANDOM_NAME, OTHER_DESCRIPTION);
		proposalHistoryManager.addProposalHistory(new ProposalHistory(firstProposalToAdd) );
		ProposalHistoryRecords proposalHistoryRecords = proposalHistoryManager.getProposalHistory(firstProposalToAdd.getId());
		List<ProposalHistory> records = proposalHistoryRecords.getProposals();
		assertThat(records.size(),is(1));
	}
	
	@Test
	public void afterAddingThreeProposalHistoryThereShouldBeThreeRecordInDb() {
		Proposal firstProposalToAdd = proposalManager.addProposal(RANDOM_NAME, OTHER_DESCRIPTION);
		proposalHistoryManager.addProposalHistory(new ProposalHistory(firstProposalToAdd) );
		proposalHistoryManager.addProposalHistory(new ProposalHistory(firstProposalToAdd) );
		proposalHistoryManager.addProposalHistory(new ProposalHistory(firstProposalToAdd) );
		ProposalHistoryRecords proposalHistoryRecords = proposalHistoryManager.getProposalHistory(firstProposalToAdd.getId());
		List<ProposalHistory> records = proposalHistoryRecords.getProposals();
		assertThat(records.size(),is(3));
	}

}
