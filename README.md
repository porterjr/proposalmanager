# Wymagane: #
* psql (testowane na 1.14.0)
* glassfish (testowane na 4.1)
* maven
 
# Instalacja: #
* uruchamiamy skrytp create_tables.sql znajdujący się w src/main/resources
* konfigurujemy poprzez konsole admina JDBC connection pool (powiązanie z bazą danych) oraz JDBC resource (powiązanie z parametrem jta-data-source w persistence xml) w glassifshu 
* w głównym katalogu projektu wpisujemy "mvn clean verify glassfish:deploy"

# Opis implementacji: #
* zbiór wniosków przechowywany jest w tabeli Proposal, historia zmian wniosków w tabeli History
* wstępna walidacja poprawności wniosku odbywa się poprzez filtr UpdateProposalFilter
* wszystkie usługi restowe umieszone są w paczce porterjr.org.bitbucket.services
* zapewniono testy unitowe z wykorzystaniem bibliotek Mockito, JunitParams, Hamcrest
* zapewniono testy integracyjne dla warstwy dao z wykorzystaniem biblioteki Arquillian

# Do zrobienia: #
* testy integracyjne dla usług restowych

# Testowanie manualne: #
* z wykorzystaniem pluginu do google chrome Advanced Rest Client Application
